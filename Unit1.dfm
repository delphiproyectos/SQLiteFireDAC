object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 301
  ClientWidth = 374
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object BtnConectar: TButton
    Left = 40
    Top = 224
    Width = 113
    Height = 49
    Caption = 'Conectar'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnClick = BtnConectarClick
  end
  object BtnEjecutar: TButton
    Left = 224
    Top = 224
    Width = 113
    Height = 49
    Caption = 'Ejecutar'
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    OnClick = BtnEjecutarClick
  end
  object TextoPantalla: TMemo
    Left = 8
    Top = 8
    Width = 358
    Height = 202
    TabOrder = 2
  end
  object FDConnection1: TFDConnection
    Left = 288
    Top = 32
  end
  object FDPhysSQLiteDriverLink1: TFDPhysSQLiteDriverLink
    Left = 288
    Top = 96
  end
end
