unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.VCLUI.Wait, FireDAC.Stan.ExprFuncs, FireDAC.Phys.SQLiteDef,
  FireDAC.Phys.SQLite, Data.DB, FireDAC.Comp.Client, FireDAC.DApt;

type
  TForm1 = class(TForm)
    BtnConectar: TButton;
    BtnEjecutar: TButton;
    TextoPantalla: TMemo;
    FDConnection1: TFDConnection;
    FDPhysSQLiteDriverLink1: TFDPhysSQLiteDriverLink;
    procedure BtnConectarClick(Sender: TObject);
    procedure BtnEjecutarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.BtnConectarClick(Sender: TObject);
begin
  TextoPantalla.Clear;
  FDConnection1.DriverName := 'SQLITE';
  FDConnection1.Params.Values['Database'] := ExtractFilePath(paramstr(0)) + '../../bbdd.db';
  try
    FDConnection1.Open;
    BtnEjecutar.Enabled := True;
    TextoPantalla.Text := 'Conexi�n establecida!';
  except on E: EDatabaseError do
    TextoPantalla.Text := 'Ha ocurrido el siguiente error: ' + E.Message;
  end;
end;

procedure TForm1.BtnEjecutarClick(Sender: TObject);
var
  consulta: TFDQuery;
begin
  consulta := TFDQuery.Create(nil);
  try
    consulta.Connection := FDConnection1;
    consulta.SQL.Text := 'SELECT * FROM usuarios;';
    consulta.Open;
    TextoPantalla.Clear;
    while not consulta.Eof do
    begin
      TextoPantalla.Lines.Add(consulta.FieldByName('id').AsString + ' - ' + consulta.FieldByName('Nombre').AsString);
      consulta.Next;
    end;
  finally
    consulta.Free;
    consulta.DisposeOf;
  end;
end;

end.
